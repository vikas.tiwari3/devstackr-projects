import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  data = [
    {
      name: 'Ideas', 
      info: ['Some random idea','This is another random idea','build an awesome application'],
    },
    {
      name:'Research',
      info: ['Lorem ipsum', 'foo', "This was in the 'Research' column"],
    },
    {
      name:'Todo',
      info: ['Get to work', 'Pick up groceries', 'Go home', 'Fall asleep'],
    },
    {
      name:'done',
      info: ['Get up','Brush teeth','Take a shower','Check e-mail','Walk dog'],
    },
  ];
}
